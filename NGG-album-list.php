<?php
/*
Plugin Name: NextGen Gallery Album Listing
Description: A plugin to display all NextGen Gallery albums with a sublisting of galleries
Version: 0.1
License: GPL
Author: Andrew Minion
Author URI: http://minionsformissions.com
*/

//   include javascript and css
wp_register_script('dropdowns.js', plugins_url().'/NGG-album-list/dropdowns.js', array('jquery'));
wp_enqueue_script( 'dropdowns.js', plugins_url().'/NGG-album-list/dropdowns.js');

wp_register_style( 'NGG-album-list.css', plugins_url().'/NGG-album-list/NGG-album-list.css' );
wp_enqueue_style( 'NGG-album-list.css', plugins_url().'/NGG-album-list/NGG-album-list.css' );

//   [NGG_album_list]

function NGG_album_list_func( $atts ) {

	//   set PHP variable
	$blog_url = str_replace(home_url(),'',site_url());
	$home_url = home_url('/');
	
	$albums_list = mysql_query("SELECT id, name, slug, sortorder FROM wp_ngg_album");
	
	echo '<ul class="dropdown">';
	
	//   list albums
	while ($album = mysql_fetch_array($albums_list))
	{
		//   display album name and dropdown arrow
		echo '<li><a class="dropdown_trigger"><span class="trigger_pointer_arrow"></span><strong>'.$album['name'].'</strong></a>
		<ul class="sub_links" style="display:none;">';
		
		//   get ids of all galleries in this album		
		$string = $album['sortorder'];
		$pattern = "/(\w:\d:\{)?\"?;?i:\d;s:\d:\"(\d+)\"?;?\}?/";
		$replacement = "$2,";
		$gallery_id = preg_replace($pattern, $replacement, $string);
		$gallery_id_array = explode(",",$gallery_id);
		
		//   get galleries and list them
		foreach ($gallery_id_array as $gallery_id) {
			//   get the galleries in this album
			$galleries_in_this_album = mysql_query("SELECT gid, slug, title FROM wp_ngg_gallery WHERE gid = '$gallery_id'");
			
			//   list the galleries from the selected album
			while ($this_gallery = mysql_fetch_array($galleries_in_this_album)) {
				echo '<li><a href="'.site_url().'/resources/photos?album='.$album['id'].'&gallery='.$this_gallery['gid'].'">'.$this_gallery['title'].'</a></li>';
			}
		}
		
		echo '</ul>
		</li>';
	}
	
	echo '</ul>';
}


add_shortcode( 'NGG_album_list', 'NGG_album_list_func' );

?>