jQuery(document).ready(function(){
	// dropdowns - switch the "Open" and "Close" state per click then slide up/down (depending on open/close state)
	jQuery("a.dropdown_trigger").click(function(){
		jQuery(this).toggleClass("active").next().slideToggle("slow");
	});
});
